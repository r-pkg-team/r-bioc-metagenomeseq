Source: r-bioc-metagenomeseq
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-metagenomeseq
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-metagenomeseq.git
Homepage: https://bioconductor.org/packages/metagenomeSeq/
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-biobase,
               r-bioc-limma,
               r-cran-glmnet,
               r-cran-rcolorbrewer,
               r-cran-matrixstats,
               r-cran-foreach,
               r-cran-matrix,
               r-cran-gplots,
               r-bioc-wrench
Testsuite: autopkgtest-pkg-r

Package: r-bioc-metagenomeseq
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R statistical analysis for sparse high-throughput sequencing
 MetagenomeSeq is designed to determine features (be it Operational
 Taxanomic Unit (OTU), species, etc.) that are differentially abundant
 between two or more groups of multiple samples. metagenomeSeq is
 designed to address the effects of both normalization and under-sampling
 of microbial communities on disease association detection and the
 testing of feature correlations.
